﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace INI
{
    class INIManager
    {
        public string Path { get; }
        public bool WriteParams(string parametr, string key)
        {
            //Function WriteParams write your parameter to "ini" file
            //args: parametr - string with name of parameter
            //key - string with your data to write in this parameter
            //return: true - if all is OK, for another 
            //For other cases it is worth catching exceptions
            string searching = parametr + "="; bool sucessfulSearch = false;
            Regex regex = new Regex(searching);
            StreamReader streamReader = new StreamReader(Path);
            string str;
            List<string> vs = new List<string>();
            while ((str = streamReader.ReadLine()) != null)
            {
                if (regex.IsMatch(str) && sucessfulSearch == false)
                {
                    str = parametr + '=' + key;
                    sucessfulSearch = true;
                }
                vs.Add(str);
            }
            if (sucessfulSearch == false)
                vs.Add(parametr + '=' + key);
            streamReader.Close();
            streamReader.Dispose();
            StreamWriter streamWriter = new StreamWriter(Path, false);
            foreach (string st in vs)
            {
                streamWriter.WriteLine(st);
            }
            streamWriter.Close();
            streamWriter.Dispose();
            return true;
        }
        public string ReadParams(string parametr)
        {
            //Function ReadParams read your parameter from "ini" file
            //args: parametr - string with name of parameter
            //return: string with the key, if the parameter is found,
            //null if the parameter if the parameter is not found
            string searching = parametr + "=";
            Regex regex = new Regex(searching);
            StreamReader streamReader = new StreamReader(Path);
            string str;
            while ((str = streamReader.ReadLine()) != null)
            {
                if (regex.IsMatch(str))
                {
                    streamReader.Close();
                    streamReader.Dispose();
                    return str.Substring(searching.Length);
                }
            }
            streamReader.Close();
            streamReader.Dispose();
            return null;
        }
        public INIManager(string pathToFile)
        {
            if (!File.Exists(pathToFile))
            {
                StreamWriter temp = File.CreateText(pathToFile);
                temp.Close(); temp.Dispose();
            }
            Path = pathToFile;
        }
    }
}
